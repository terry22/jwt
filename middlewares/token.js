const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, creds) => {
    if (err) {
      if (err.name == "TokenExpiredError")
        return res.status(401).send({ error: "Please relogin!" });
      return res.sendStatus(401);
    }
    try {
      // Add a expiry check here next time
      req.creds = creds;
      next();
    } catch (error) {
      console.log(error);
      return res.sendStatus(500);
    }
  });
};

profiles = [
  {
    userId: 0,
    name: "Jun Xian",
    age: 25,
    title: "Backend Developer",
  },
  {
    userId: 1,
    name: "Jia Hwee",
    age: 99,
    title: "Data Scientist",
  },
];

exports.getProfile = (req, res) => {
  let profile;

  try {
    // Check here
    if (
      +req.params.userId !== +req.creds.userId &&
      req.creds.role !== "admin"
    ) {
      return res.sendStatus(403);
    }
    profile = profiles.filter(
      (profile) => +profile.userId === +req.params.userId
    );
    res.json(profile);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.getProfiles = (req, res) => {
  try {
    // Check here
    if (req.creds.role !== "admin") {
      return res.sendStatus(403);
    }
    res.json(profiles);
  } catch (error) {
    return res.sendStatus(500);
  }
};

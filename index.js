const express = require("express");
const app = express();

const cookieParser = require("cookie-parser");

const authRoutes = require("./routes/auth");
const profilesRoutes = require("./routes/profiles");
const Token = require("./middlewares/token");

require("dotenv").config();
app.use(express.json());
app.use(cookieParser());

app.use(authRoutes);
app.use(Token); // Middleware
app.use(profilesRoutes);

app.listen(process.env.PORT, () =>
  console.log(`Running on port ${process.env.PORT}`)
);

// Method to use to generate TOKEN SECRET:
// Using node in terminal, require('crypto').randomBytes(64).toString('hex')

const express = require("express");

const profilesController = require("../controllers/profiles");

const router = express.Router();

router.get("/profiles/:userId", profilesController.getProfile);
router.get("/profiles", profilesController.getProfiles);

module.exports = router;
